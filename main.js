//load express after you have done npm install */
var express = require("express");
//create an application
var app = express();

// tells express that "/public directory" is the document root
app.use(express.static(__dirname + "/public"));

console.log("_dirname = " + __dirname);

//start out web server on port 3000
app.listen(3000, function(){
    console.info("my web server has started on port 3000");
    console.info("document root is at "+ __dirname + "public");
    
});

//in terminal, run "node main.js"
// go to webserver http://localhost:3000/

// install nodemon in terminal, first exit server by ctrl c, then enter sudp npm install -g nodemon
// run nodemon by entering in terminal: "nodemon" (only needs to be installed and run once)

// nodemon main.js to run the server before viewing the web address